#include "test_data.h"

#include "num_pipeline_sband_freq_counter/num_pipeline_sband_freq_counter.h"
#include "check_data.h"

#include <gtest/gtest.h>

namespace {

  using namespace Numurus;
  using namespace PLSbandFreqCounter_Test;

  TEST(TestFunction, TestMinRpm)
  {
    PLSbandFreqCounter example;
    example.checkpoint.setPrefix(std::string(::testing::UnitTest::GetInstance()->current_test_info()->name()) + "_");

    PLRoutine::Config config;

    const int MinRpms = 60;
    config.name_ = "TestMinRpm";
    config.description_ = "Configuration from TestFunction::TestMinRpm";
    config.int_params[ PLSbandFreqCounter::PLSbandFreqCounterConfig::MinRpmsName ] = MinRpms;

    ASSERT_TRUE( example.configure( config ) );

    cv::Mat in( testData<int>() ), out;
    float qualIn = 0.0, qualOut = 0.0;
    auto retval = example.process( &in, &out, qualIn, &qualOut );
    ASSERT_EQ( retval, true );

    // Expectation of 4 results is based on apriori testing
    //checkSignalTestDataResult( out, 1 );

  }

}
