
#include <vector>
#include <algorithm>

#include <opencv2/imgproc/imgproc.hpp>

#include "num_pipeline_sband_freq_counter/num_pipeline_sband_freq_counter.h"

#ifdef __DEBUG_SBAND_DETECTOR
#include <sstream>
#endif

namespace Numurus
{

  // Default values for SBand Frequency Counter configuration values
  const int   PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultMinRpms = 1;
  const int   PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultXcorrReps = 1;
  const float PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultMaxProportion = 0.5;
  const int   PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultMaxResults = 10;

  const std::string PLSbandFreqCounter::PLSbandFreqCounterConfig::XcorrRepsName = "xcorr_reps";
  const std::string PLSbandFreqCounter::PLSbandFreqCounterConfig::MinRpmsName = "min_rpms";
  const std::string PLSbandFreqCounter::PLSbandFreqCounterConfig::MaxProportionName = "max_proportion";
  const std::string PLSbandFreqCounter::PLSbandFreqCounterConfig::MaxResultsName = "max_results";



  using cv::Mat;

  PLSbandFreqCounter::PLSbandFreqCounter()
    : PLRoutine(),
      freqConfig_()
  {
    // Set reasonable defaults
    config_.name_ = "PLSbandFreqCounter";
    config_.description_ = "Counts frequency patterns in SBand event detection data.";
  }

  PLSbandFreqCounter::~PLSbandFreqCounter()
  {;}

  bool PLSbandFreqCounter::configure(const PLRoutine::Config &newConfig)
  {
    if( !freqConfig_.configure(newConfig) ) return false;

    return PLRoutine::configure(newConfig);
  }

  bool PLSbandFreqCounter::process(Mat *data_in, Mat *data_out, float quality_in, float *quality_out)
  {
      // As a fallback, leave the quality unchanged
      *quality_out = quality_in;

      // Working data format is an int vector of length t, convert all other
      // data formats to this shape
      if( data_in == nullptr || data_out == nullptr ) {
        log( PL_ERROR, "Received null ptr for input / output data");
        return false;
      }

      if( !data_in->isContinuous() ) {
        log( PL_ERROR, "Received null ptr for input / output data");
        return false;
      }

      // Reshape needs to be more flexible for ND arrays
      const auto sz = data_in->size;

      Mat data_flat;

      if( data_in->dims == 2 ) {
        data_flat = data_in->reshape(1,1);
      } else {
        int num_elem = 1;
        for( int i = 0; i < data_in->dims; ++i ) num_elem *= sz[i];

        data_flat = cv::Mat( 1, num_elem, data_in->type(), data_in->data );
      }

      checkpoint( data_flat, "flat" );

      Mat data( data_flat.size(), CV_8U );
      cv::compare( data_flat, 0, data, cv::CMP_GT );
      data = data / 255;

      checkpoint( data, "as_bool");

      // Perform cross-correlation using OpenCV function
      // Output is CV_16S
      for( int i = 0; i < freqConfig_.xcorrReps; ++i ) {
        cv::filter2D( data, data, CV_16S, data, cv::Point(-1,-1), 0, cv::BORDER_WRAP );

        double min, max;
        cv::minMaxLoc( data, &min, &max );

        const int newMax = 16;
        if( max > newMax ) {
          int scalar = floor(max/newMax);
          data = data/scalar;
        }

#ifdef __DEBUG_SBAND_DETECTOR
        {
          char name[20];
          snprintf( name, 19, "xcorr_rep%d", i );
          checkpoint( data, name);
        }
#endif
      }

      // Take only half of the data
      const int numElem = data_in->total();
      const int numElem2 = numElem/2;
      data = Mat( data, cv::Rect(numElem2,0,numElem-numElem2,1) );

      checkpoint( data, "xcorr_final");

      // Extract data up to an expected max RPMs
      if( freqConfig_.minRpms > 0 ) {
        const int maxBin = ceil( float(60.0/freqConfig_.minRpms) * SBandDetectorHz );
        const int maxBinLim = std::min( maxBin, data.cols );

        if( maxBinLim == 0 ) {
          log( PL_ERROR, "minRpms too high, retained no data");
          return false;
        }

  #ifdef __DEBUG_SBAND_DETECTOR
        {
          std::string debug;
          {
            std::ostringstream debugStr(debug);
            debugStr << "Retaining only the lowest " << maxBinLim << " bins";
          }
          log( PL_DEBUG, debug );
        }
  #endif

        data = Mat( data, cv::Rect(0,0, maxBinLim, 1));
      }

      checkpoint( data, "minrpms");

      double maxVal;

      // Want to find max other than at DC
      cv::Mat roi( data, cv::Range::all(), cv::Range(1,data.cols));
      cv::minMaxIdx( roi, NULL, &maxVal, NULL, NULL );
      const double threshold = maxVal * freqConfig_.maxProportion;

      // Find peaks, only retain peaks greater than threshold
      const int cols = data.size().width - 1;

      struct Result {
        int idx;
        int16_t val;

        Result() = delete;

        Result( int i, int16_t v )
          : idx(i), val(v) {;}
      };

      std::vector< Result > out;

      // Start at 1, ignore DC result
      for( int i = 1; i < cols; ++i ) {

        const auto val = data.at<int16_t>(0,i);

        if( (val >= data.at<int16_t>(0,i-1)) &&
            (val > data.at<int16_t>(0,i+1)) ) {

          if( val >= threshold ) {
            out.push_back( Result(i, val ) );
          }

        }
      }

      std::sort( out.begin(), out.end(),
              []( const Result &a, const Result &b ) { return a.val > b.val;  });

      if( freqConfig_.maxResults > 0 ) {
        while( out.size() > freqConfig_.maxResults ) { out.pop_back(); }
      }

      std::vector< int > outVec;

      std::transform( out.begin(), out.end(), back_inserter(outVec),
                      []( const Result &result ) -> int { return result.idx; } );


      // To comply with the new structure, create a 4D output
      std::vector<int> outSize(4,1);
      outSize[0] = outVec.size();

      Mat outMat( outSize, CV_32S, outVec.data() );

      checkpoint( outMat, "final");

      // Output format is 16S

      outMat.convertTo(*data_out, CV_16S);

      if( freqConfig_.maxResults > 0 ) {
        *quality_out = float(outVec.size()) / freqConfig_.maxResults;

        // Bound
        if( *quality_out > 1.0 ) *quality_out = 1.0;
        if( *quality_out < 0.0 ) *quality_out = 0.0;
      }


      return true;

  }

  //==

  bool PLSbandFreqCounter::PLSbandFreqCounterConfig::configure(const PLRoutine::Config &newConfig)
  {
    // Extract configuration from newConfig's maps
    if( newConfig.int_params.count( XcorrRepsName ) > 0 ) {
      const int newXcorrReps = newConfig.int_params.at(XcorrRepsName);

      if( newXcorrReps < 1 || newXcorrReps > 4 ) {
        //log( PL_ERROR, "Specified xcorrReps out of bounds");
        return false;
      }

      xcorrReps = newXcorrReps;
    }

    if( newConfig.int_params.count( MinRpmsName ) > 0 ) {
      const int newMinRpms = newConfig.int_params.at(MinRpmsName);

      if( newMinRpms <= 0 ) {
        //log( PL_ERROR, "Requested minRpms < 0");
        return false;
      }

      minRpms = newMinRpms;
    }

    if( newConfig.float_params.count( MaxProportionName ) > 0 ) {
      const float newMaxProportion = newConfig.float_params.at(MaxProportionName);

      if( (newMaxProportion < 0)  || (newMaxProportion > 1.0) ) {
        //log( PL_ERROR, "Requested minRpms < 0");
        return false;
      }

      maxProportion = newMaxProportion;
    }

    if( newConfig.int_params.count( MaxResultsName ) > 0 ) {
      const int newMaxResults = newConfig.int_params.at(MaxResultsName);

      if( newMaxResults < 0 ) {
        //log( PL_ERROR, "Requested minRpms < 0");
        return false;
      }

      maxResults = newMaxResults;
    }

    return true;
  }


}
