#include "test_data.h"

#include "num_pipeline_sband_freq_counter/num_pipeline_sband_freq_counter.h"

#include <gtest/gtest.h>

namespace {

  using namespace Numurus;
  using namespace PLSbandFreqCounter_Test;

  void CheckDefaultConfig( const PLSbandFreqCounter::PLSbandFreqCounterConfig &config ) {
    ASSERT_EQ( PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultMinRpms, config.minRpms );
    ASSERT_EQ( PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultXcorrReps, config.xcorrReps );
    ASSERT_EQ( PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultMaxResults, config.maxResults );
    ASSERT_EQ( PLSbandFreqCounter::PLSbandFreqCounterConfig::DefaultMaxProportion, config.maxProportion );
  }

  TEST(TestConfig, TestDefaults)
  {
    PLSbandFreqCounter::PLSbandFreqCounterConfig config;

    CheckDefaultConfig( config );
  }

  TEST( TestConfig, TestEmptyConfig )
  {
    PLSbandFreqCounter::PLSbandFreqCounterConfig config;

    PLRoutine::Config newConfig;

    ASSERT_EQ( true, config.configure(newConfig) );

    CheckDefaultConfig( config );
  }

  TEST( TestConfig, TestSetGoodConfig )
  {
    PLSbandFreqCounter::PLSbandFreqCounterConfig config;
    PLRoutine::Config newConfig;

    CheckDefaultConfig( config );

    const int NewMinRpms = 30;
    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MinRpmsName] = NewMinRpms;

    const float NewMaxProportion = 0.5;
    newConfig.float_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MaxProportionName] = NewMaxProportion;

    const int NewXcorrReps = 2;
    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::XcorrRepsName] = NewXcorrReps;

    const int NewMaxResults = 15;
    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MaxResultsName] = NewMaxResults;

    ASSERT_TRUE( config.configure(newConfig) );

    ASSERT_EQ( NewMinRpms, config.minRpms );
    ASSERT_EQ( NewXcorrReps, config.xcorrReps );
    ASSERT_EQ( NewMaxResults, config.maxResults );
    ASSERT_EQ( NewMaxProportion, config.maxProportion );

  }

  TEST( TestConfig, TestSetBadMaxProportionConfig )
  {
    PLSbandFreqCounter::PLSbandFreqCounterConfig config;
    PLRoutine::Config newConfig;

    CheckDefaultConfig( config );

    newConfig.float_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MaxProportionName] = -0.1;
    ASSERT_FALSE( config.configure(newConfig) );
    CheckDefaultConfig( config );

    newConfig.float_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MaxProportionName] = 1.1;
    ASSERT_FALSE( config.configure(newConfig) );
    CheckDefaultConfig( config );
  }

  TEST( TestConfig, TestSetBadMinRpm )
  {
    PLSbandFreqCounter::PLSbandFreqCounterConfig config;
    PLRoutine::Config newConfig;

    CheckDefaultConfig( config );

    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MinRpmsName] = 0;
    ASSERT_FALSE( config.configure(newConfig) );
    CheckDefaultConfig( config );

    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MinRpmsName] = -1;
    ASSERT_FALSE( config.configure(newConfig) );
    CheckDefaultConfig( config );

  }

  TEST( TestConfig, TestSetBadMaxResults )
  {
    PLSbandFreqCounter::PLSbandFreqCounterConfig config;
    PLRoutine::Config newConfig;

    CheckDefaultConfig( config );

    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::MaxResultsName] = -1;
    ASSERT_FALSE( config.configure(newConfig) );
    CheckDefaultConfig( config );

  }

  TEST( TestConfig, TestSetBadXcorrReps )
  {
    PLSbandFreqCounter::PLSbandFreqCounterConfig config;
    PLRoutine::Config newConfig;

    CheckDefaultConfig( config );

    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::XcorrRepsName] = 0;
    ASSERT_FALSE( config.configure(newConfig) );
    CheckDefaultConfig( config );

    newConfig.int_params[PLSbandFreqCounter::PLSbandFreqCounterConfig::XcorrRepsName] = -1;
    ASSERT_FALSE( config.configure(newConfig) );
    CheckDefaultConfig( config );

  }

}
