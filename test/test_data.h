#pragma once

#ifndef TEST_DATA_PATH
  #error "TEST_DATA_PATH must be defined for unit test data to be found"
#endif

#include "test_data_impl.h"


/// Test data sets are stored within a top-level dict within the test data JSON
/// file, with string keys.
#define TEST_DATA_ZEROS   "zeros"
#define TEST_DATA_RANDOM  "random"
#define TEST_DATA_SIGNAL  "signal"

namespace PLSbandFreqCounter_Test {

  struct SbandFreqCounterTestData : public NumPlTest::TestData {

    SbandFreqCounterTestData()
      : TestData( TEST_DATA_PATH"/sband_test_data.json" )
    {;}

  };



  /* Define a singleton test data instance which is available across
   * test cases.
   *
   * The singleton reduces the boilerplate in each test case,
   * and lets us cache the test data.   Technically,
   * use of the singleton isn't mandatory, could just load the
   * data in each test case.
   *
   * \todo Perhaps this could be done with a GoogleTest
   *       fixture instead?
   */
  inline SbandFreqCounterTestData &theInstance() {
    static SbandFreqCounterTestData testDataClass;
    return testDataClass;
  }

  template <typename T>
  cv::Mat_<T> testData( const std::string &key=TEST_DATA_SIGNAL ) {
    return theInstance().operator()<T>( key );
  }

  inline size_t testDataLen( const std::string &key=TEST_DATA_SIGNAL ) {
    return testData<int>( key ).total();
  }

}
