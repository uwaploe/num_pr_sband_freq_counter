#pragma once

#ifdef __DEBUG_SBAND_DETECTOR

#include <opencv2/core/persistence.hpp>

namespace Checkpoint {

  struct CheckpointObj {
    CheckpointObj( const CheckpointObj & ) = delete;

    CheckpointObj()
      : checkpointInitialized_(false),
        prefix_("") {;}

    CheckpointObj(const std::string &prefix)
      : checkpointInitialized_(false),
        prefix_(prefix) {;}

    void operator()( const cv::Mat &mat, const std::string &varname ) {
      int flags = cv::FileStorage::WRITE;
      if( checkpointInitialized_ ) flags |= cv::FileStorage::APPEND;

      const std::string filename = prefix_ + varname + ".yaml";

      cv::FileStorage out( filename, flags );
      out << varname << mat;

      checkpointInitialized_ = true;
    }

    void setPrefix( const std::string &prefix ) {
      prefix_ = prefix;
    }

    bool checkpointInitialized_;
    std::string prefix_;
  };

}

#else

namespace Checkpoint {

  struct CheckpointObj {
    CheckpointObj() {;}

    CheckpointObj( const CheckpointObj & ) = delete;

    CheckpointObj( const std::string &prefix ) {;}

    void operator()( const cv::Mat &mat, const std::string &varname ) {;}
    void setPrefix( const std::string &prefix ) {;}
  };

}

#endif
