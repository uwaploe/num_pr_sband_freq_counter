#pragma once

#include "test_data.h"

#include <gtest/gtest.h>

namespace PLSbandFreqCounter_Test {

  // This is known apriori based on the "signal" test data set run
  // against the default detector options
  static const unsigned int SignalTestDataExpectedResults = 10;

  static void checkSignalTestDataResult( const cv::Mat &data_out, unsigned int expectedResults = SignalTestDataExpectedResults )
  {

    ASSERT_EQ( data_out.type(), CV_16S );

    cv::Size sz( data_out.size() );

    ASSERT_EQ( sz.width, 1 );
    ASSERT_EQ( sz.height, expectedResults );
  }


}
