#include "test_data.h"

#include "num_pipeline_sband_freq_counter/num_pipeline_sband_freq_counter.h"
#include "check_data.h"

#include <gtest/gtest.h>

namespace {

  using namespace Numurus;
  using namespace PLSbandFreqCounter_Test;

  TEST(TestFunction, ProcessDataOneAsInt)
  {
    PLSbandFreqCounter example;
    example.checkpoint.setPrefix(std::string(::testing::UnitTest::GetInstance()->current_test_info()->name()) + "_");

    cv::Mat in( testData<int>() ), out;
    float qualIn = 0.0, qualOut = 0.0;
    auto retval = example.process( &in, &out, qualIn, &qualOut );
    ASSERT_EQ( retval, true );
    ASSERT_FLOAT_EQ( qualOut, 1.0 );

    checkSignalTestDataResult( out );
  }

  TEST(TestFunction, ProcessDataOneAsDouble)
  {
    PLSbandFreqCounter example;
    example.checkpoint.setPrefix(std::string(::testing::UnitTest::GetInstance()->current_test_info()->name()) + "_");

    cv::Mat in( testData<double>() ), out;
    float qualIn = 0.0, qualOut = 0.0;
    auto retval = example.process( &in, &out, qualIn, &qualOut );
    ASSERT_EQ( retval, true );
    ASSERT_FLOAT_EQ( qualOut, 1.0 );

    checkSignalTestDataResult( out );
  }

  TEST(TestFunction, ProcessDataOneAsUnsignedChar)
  {
    PLSbandFreqCounter example;
    example.checkpoint.setPrefix(std::string(::testing::UnitTest::GetInstance()->current_test_info()->name()) + "_");

    cv::Mat in( testData<unsigned char>() ), out;
    float qualIn = 0.0, qualOut = 0.0;
    auto retval = example.process( &in, &out, qualIn, &qualOut );
    ASSERT_EQ( retval, true );
    ASSERT_FLOAT_EQ( qualOut, 1.0 );

    checkSignalTestDataResult( out );
  }

}
