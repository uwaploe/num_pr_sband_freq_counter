#!/usr/bin/env python3

import numpy as np
import cv2
import scipy.stats as stats

import json

def mat_to_json( mat, shape=None ):

    if not shape:
        shape = mat.shape

    while(len(shape) < 4):
        shape = shape + (1,)

    m = { 'samples': shape[0],
          'rows': shape[1],
          'cols': shape[2],
          'channels': shape[3],
          'dtype': str(mat.dtype),
          'data': str(mat.tolist()) }

    return m


def rpm2radpersec(rpm):
    return rpm/60*(2*np.pi)



if __name__ == '__main__':

    # Dict to accumulate test data.  Serialized to file at end of main()
    test_data = {}

    ## Detector sampling freq and length of buffer
    sband_hz=100
    buffer_len_secs=60

    ## Fixed random seed for debugging
    np.random.seed(0)

    buffer_len = sband_hz*buffer_len_secs

    # ***** Generate the all-zeroes data set
    test_data['zeros'] = mat_to_json( np.zeros( (buffer_len), dtype=np.int8 ) )


    # ***** Generate a random data set
    test_data['random'] = mat_to_json( np.random.randint(0,2, size=(buffer_len), dtype=np.int8 ) )

    # ***** Generate the simulated data set

    ## Emitters are defined by a rate in rpm and an initial phase (in degrees).
    ## Phase is the bearing from the detector to the emitter (or is it the reciprocal?)
    ## Emitters are pointed at detector when (wt + phase) == pi

    signals = [[12, 0], [12.2,43] ]

    ## Transform signals to rad/sec and radians phase

    signals = [[rpm2radpersec(a[0]), np.radians(a[1])] for a in signals]

    detector_mean = np.pi

    ## Variance defnes the angular "beamwidth" of the detector
    detector_var  = 0.1

    ## Maximum detection probability.  If < 1, detector can give false negative
    p_detection_max = 1.0

    ## prob of random detection, if > 0, detector can give false positive
    p_detection_random = 0.00

    buffer = np.zeros((buffer_len))
    t = np.arange(0,buffer_len) / sband_hz

    for i,sig in enumerate(signals):
        angle = (sig[0]*t + sig[1])%(2*np.pi)

        p_detect = stats.norm.pdf(angle,detector_mean,detector_var) * p_detection_max / stats.norm.pdf(0,0,detector_var)

        ## Add a constant probability of random noise
        p_detect = np.clip( p_detect, p_detection_random, None )

        ## Actually draw samples for detection
        prob = np.random.random(buffer_len)

        ## And add to the detection buffer
        buffer = buffer+np.clip( np.sign(p_detect-prob), 0, 1 )

    ## Normalize to down to 0-1
    buffer = np.clip( buffer, 0, 1).astype( np.int8 )

    test_data['signal'] = mat_to_json( buffer )


    ### Write to file
    with open( "sband_test_data.json", 'w' ) as f:
      json.dump( test_data, f, indent=2 )
