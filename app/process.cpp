
#include <iostream>

#include <opencv2/core.hpp>

#include <boost/filesystem.hpp>

#include "test_data_impl.h"

#include "num_pipeline_sband_freq_counter/num_pipeline_sband_freq_counter.h"

using std::cout;
using std::cerr;
using std::endl;

using namespace Numurus;

/// Very simple app that loads a NEPI JSON file, processes it with the PR,
/// and outputs the result.
int main( int argc, char **argv ) {

  if( argc < 2 ) {
    cerr << "Usage:  process [NEPI JSON input file]" << endl;
    exit(0);
  }

  const std::string inputfile( argv[1] );

  cout << "Processing file " << inputfile << endl;

  // Check for existence of file
  if( !boost::filesystem::is_regular_file(inputfile) ) {
    cerr << " Hm, file " << inputfile << " doesn't exist" << endl;
    exit(-1);
  }

  // Load the data
  cv::Mat inputMat( NumPlTest::LoadJsonToMat( inputfile ) );
  if( inputMat.empty() ) {
    cerr << "Error loading file " << inputfile << endl;
    exit(-1);
  }

  cout << "Input is size ";
  for( int i = 0; i < inputMat.dims; ++i ) {
    cout << inputMat.size[i] << " ";
  }
  cout << endl;


  PLSbandFreqCounter proc;

  cv::Mat outputMat;
  float qualIn = 0.0, qualOut = 0.0;
  auto retval = proc.process( &inputMat, &outputMat, qualIn, &qualOut );

  if( retval == false ) {
    cerr << "Process returned false" << endl;
  }

  int totalData = 1;
  cout << "Output is size ";
  for( int i = 0; i < outputMat.dims; ++i ) {
    cout << outputMat.size[i] << " ";
    totalData *= outputMat.size[i];
  }
  cout << endl;

  // Create a flat 1-D matrix for output
  cv::Mat flattened( 1, totalData, outputMat.type(), outputMat.data );

  cout << "Output: " << endl << flattened << endl;



  exit(0);
}
