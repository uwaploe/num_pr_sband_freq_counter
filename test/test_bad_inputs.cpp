#include "num_pipeline_sband_freq_counter/num_pipeline_sband_freq_counter.h"
#include <gtest/gtest.h>

namespace {

  using namespace Numurus;

  // Test with NULL data in
  TEST(TestBadInput, NullDataIn)
  {
    PLSbandFreqCounter example;

    cv::Mat out;
    float qualIn = 0.0, qualOut = 0.0;

    auto retval = example.process( nullptr, &out, qualIn, &qualOut );

    ASSERT_EQ( retval, false );
  }

  // Test with NULL data out
  TEST(TestBadInput, NullDataOut)
  {
    PLSbandFreqCounter example;

    cv::Mat in;
    float qualIn = 0.0, qualOut = 0.0;

    auto retval = example.process( &in, nullptr, qualIn, &qualOut );

    ASSERT_EQ( retval, false );
  }

}
