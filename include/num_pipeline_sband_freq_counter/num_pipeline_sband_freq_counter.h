#pragma once

#include "pl_routine.h"

#include "checkpoint.h"

// If defined, debug output is enabled
//#define __DEBUG_SBAND_DETECTOR

namespace Numurus {

class PLSbandFreqCounter : public PLRoutine
{
public:

 static const int SBandDetectorHz = 10;

  // \brief Structure to store configuration
  //
  struct PLSbandFreqCounterConfig {

    // Default configuration values
    static const int DefaultMinRpms;
    static const int DefaultXcorrReps;
    static const float DefaultMaxProportion;
    static const int DefaultMaxResults;

    static const std::string XcorrRepsName;
    static const std::string MinRpmsName;
    static const std::string MaxProportionName;
    static const std::string MaxResultsName;



    // Default constructor sets reasonable defaults
    PLSbandFreqCounterConfig()
      : xcorrReps(DefaultXcorrReps),
        minRpms(DefaultMinRpms),
        maxProportion( DefaultMaxProportion ),
        maxResults( DefaultMaxResults )
    {;}

    //! Extracts relevant configuration info from
    //! maps within PLRoutine::Config.
    //!
    //! \return frue if successful, false if a value is out of bounds
    virtual bool configure(const PLRoutine::Config &config);

    //! Number of cross-correlation iterations to perform on the data.
    //! Must be between 1 and 4.
    int xcorrReps;

    //!
    int minRpms;

    //! PLSbandFreqCounter::process returns autocorrelation peaks within
    //! maxProportion of the amplitude of the highest peak.
    float maxProportion;

    //! Maximum number of results to return.
    int maxResults;

  };

  //! Default constructor
  PLSbandFreqCounter();

  //! Destructor
  virtual ~PLSbandFreqCounter();

  //! \brief Configure this PR from a PLRoutine::Config.
  //!
  //! \return true on success, false on an error (parameter out of bounds)
  virtual bool configure(const PLRoutine::Config &config);

  //! Process data.
  virtual bool process(cv::Mat *data_in, cv::Mat *data_out, float quality_in, float *quality_out);

  //! Only used when testing algorithm
  Checkpoint::CheckpointObj checkpoint;

protected:

  //! Local copy of configuration values
  PLSbandFreqCounterConfig freqConfig_;

};

}
